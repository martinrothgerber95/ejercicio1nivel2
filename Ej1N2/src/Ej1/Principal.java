package Ej1;

import java.util.Scanner;

public class Principal {

	public static void main(String[] args) {
		
		Scanner entrada = new Scanner(System.in);
		int movimientos =0;
		boolean ver;
		String sentido;
		int i=0;
		
		Robot mover = new Robot("Norte");
		
		System.out.println("Ingresa H para mover el robot en sentido Horario o A para mover el robot en sentido Antihorario");
		
		sentido = entrada.next();
		do {
			switch (sentido.charAt(i)) {
			
				case 'H':
				case 'h': mover.girarHorario();  movimientos++; i++;break;
				case 'A':
				case 'a': mover.grirarAntihorario(); movimientos++; i++; break;
				default : i++; break;
			}
			
			ver= mover.verificar(); 
			
		}while(i<1000 && ver!= true && i < sentido.length());
	
		
		if( ver== true) {
			System.out.println("cantidad de movimientos: "+movimientos);
		}else
			System.out.println("cantidad de movimientos: -1");
	}

}
