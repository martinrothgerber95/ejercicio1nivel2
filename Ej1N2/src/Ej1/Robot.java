package Ej1;

public class Robot {
	
	private String posicion;
	boolean verificar=false;
	
	//constructor
		public Robot(String posicion) {
			
			this.posicion = posicion;
		}

		//gira en sentido hoario
		public void girarHorario() {
			switch (posicion) {
		
		case "Norte": posicion = "Este"; break;
		case "Este": posicion = "Sur"; break;
		case "Oeste": posicion = "Norte"; break;
		
		}
		}
		
		// gira en sentido antihorario
		public void grirarAntihorario() {
			switch (posicion) {
		
			case "Norte": posicion = "Oeste"; break;
			case "Este": posicion = "Norte"; break;
			case "Oeste": posicion = "Sur"; break;
		
		}
		}
		
		// verifica si esta en la posicion sur
		public boolean verificar() {
			if ( posicion == "Sur") {
				verificar = true;
			}
			return verificar;
			
		}
		
		public String getPosicion() {
		return posicion;
		}
		
}
